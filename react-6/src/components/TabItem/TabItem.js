import React, {Component} from 'react';
import {Route} from "react-router";

import '../../App.css';

class TabItem extends Component {
    render() {
        return (
            <Route path={'/' + this.props.path} exact={true} render={() => (
                <div>
                    <img className={'tab-image'} src={this.props.source} alt=""/>
                    <p className={'tab-text'}>{this.props.routeText}</p>
                </div>)}/>
        );
    }
}

export default TabItem;