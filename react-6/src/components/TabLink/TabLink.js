import React, {Component} from 'react';
import {Link} from "react-router-dom";

import '../../App.css';

class TabLink extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className={'tab-link'}>
                <Link className={'link'} to={this.props.link}>{this.props.linkText}</Link>
            </div>
        );
    }
}

export default TabLink;