import React, {Component} from 'react';
import {Link, Route} from "react-router-dom";

import TabLink from "./components/TabLink/TabLink";
import TabItem from "./components/TabItem/TabItem";

const Home = () => <h1>Home page tagecdcfsds</h1>
const LogIn = () => <h1>Log in page tagecdcfsds</h1>
const Register = () => <h1>Register page tagecdcfsds</h1>

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            links: [
                {
                    linkEnd: 'web',
                    itemLinkText: "Web Design",
                    itemDescription: "Web Design is good but not for me",
                    source: "https://images.unsplash.com/photo-1508138221679-760a23a2285b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1000&q=80"
                },
                {
                    linkEnd: 'graphic',
                    itemLinkText: "GraphicDesign",
                    itemDescription: "Graphic Design is good but not for me",
                    source: "https://i.redd.it/6nl1sucb1d041.jpg"
                },
                {
                    linkEnd: 'seo',
                    itemLinkText: "SEO Service",
                    itemDescription: "SEO Service is good but not for me",
                    source: "https://www.mandysam.com/img/random.jpg"
                }

            ]
        }
    }
    render() {
        let links = this.state.links.map((item, key) => <TabLink index={key} link={item.linkEnd} linkText={item.itemLinkText}/>);
        let descriptions = this.state.links.map((item, key) => <TabItem index={key} path={item.linkEnd} routeText={item.itemDescription} source={item.source}/>);
        return (
            <>
                {/*<Link to={'/user'}>Home</Link>*/}
                {/*<Link to={'/login'}>Log In</Link>*/}
                {/*<Link to={'/register'}>Register</Link>*/}
                {/*<Link to={'/'}>Root</Link>*/}


                {/*<Route path={'/user'} component={Home} />*/}
                {/*<Route path={'/login'} component={LogIn} />*/}
                {/*<Route path={'/register'} component={Register} />*/}
                {/*<Route path={'/'} exact={true} render={() => (<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Architecto at aut doloremque dolores laboriosam molestias odit optio vero. Eius iure, magnam numquam odio*/}
                {/*    pariatur provident quisquam reiciendis sunt! Expedita, minima!</p>)}/>*/}


                {links}
                {descriptions}



            </>
        );
    }
}

export default App;
